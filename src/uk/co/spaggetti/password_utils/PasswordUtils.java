package uk.co.spaggetti.password_utils;

public class PasswordUtils {

	static {
		System.loadLibrary("password-utils");
	}

	/**
	 * Charset flags for use in password generation.
	 */
	public class CharSet {

		public static final int NUMBER = 0x01;
		public static final int SPECIAL = 0x02;
		public static final int UPPER_CASE = 0x04;
		public static final int LOWER_CASE = 0x08;
		public static final int RESTRICTED_SYMBOL = 0x10;
	}

	/**
	 * Generate a random password.
	 * 
	 * @param length
	 * @param characterSets
	 *            Flags of character sets in {@link CharSet} to use.
	 * @return
	 * @see PasswordUtils#generatePronouceable(int, int)
	 */
	public static native String generateRandom(int length, int characterSets);

	/**
	 * Generates a pronounceable password.
	 * 
	 * @param minLength
	 * @param maxLength
	 * @param characterSets
	 *            Flags of character sets in {@link CharSet} to use. Note that
	 *            {@link CharSet#LOWER_CASE} is always used, regardless of the
	 *            flag specified here.
	 * @return
	 * @see PasswordUtils#generateRandom(int, int)
	 */
	public static native String generatePronouceable(int length, int characterSets);

	/**
	 * Estimates the strength of a password.
	 * 
	 * @return The estimated strength, in bits.
	 */
	public static native float estimateStrength(String password);
}
