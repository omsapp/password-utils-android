#include <jni.h>
#include <malloc.h>
#include "uk_co_spaggetti_password_utils_PasswordUtils.h"
#include <android/log.h>
#include <math.h>
#include "pass-utils/src/passutils.h"

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))

void ThrowRuntimeException(JNIEnv *env, const char* message) {
	jclass clazz = env->FindClass("java/lang/RuntimeException");
	env->ThrowNew(clazz, message);
}

/*
 * Class:     uk_co_spaggetti_password_utils_PasswordUtils
 * Method:    generateRandom
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_uk_co_spaggetti_password_1utils_PasswordUtils_generateRandom(
		JNIEnv *env, jclass clazz, jint jLength, jint jCharsets) {

	const char * password = GenerateRandom(jLength, jCharsets);
	if (!password) {
		ThrowRuntimeException(env, "Failed to generate password");
	}

	jstring jPassword = env->NewStringUTF(password);
	free((void*) password);
	return jPassword;
}

/*
 * Class:     uk_co_spaggetti_password_utils_PasswordUtils
 * Method:    generatePronouceable
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_uk_co_spaggetti_password_1utils_PasswordUtils_generatePronouceable(
		JNIEnv *env, jclass clazz, jint jLength, jint jCharsets) {

	const char * password = GeneratePronouceable(jLength, jCharsets);
	if (!password) {
		ThrowRuntimeException(env, "Failed to generate password");
	}

	jstring jPassword = env->NewStringUTF(password);
	free((void*) password);
	return jPassword;
}

/*
 * Class:     uk_co_spaggetti_password_utils_PasswordUtils
 * Method:    estimateStrength
 * Signature: (Ljava/lang/String;)F
 */
JNIEXPORT jfloat JNICALL Java_uk_co_spaggetti_password_1utils_PasswordUtils_estimateStrength(
		JNIEnv *env, jclass clazz, jstring jPassword) {

	const char * const password = env->GetStringUTFChars(jPassword, 0);
	int len = env->GetStringLength(jPassword);

	return EstimateStrength(password, len);
}
