LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := password-utils
LOCAL_SRC_FILES := \
./password-utils.cpp \
./pass-utils/src/passutils.c \
./pass-utils/src/apg/bloom.c \
./pass-utils/src/apg/sha/sha.c \
./pass-utils/src/apg/cast/cast.c \
./pass-utils/src/apg/rnd.c \
./pass-utils/src/apg/pronpass.c \
./pass-utils/src/apg/randpass.c \
./pass-utils/src/apg/restrict.c \
./pass-utils/src/apg/errors.c \
./pass-utils/src/apg/getopt.c \
./pass-utils/src/apg/convert.c

LOCAL_CFLAGS := -std=c99

include $(BUILD_SHARED_LIBRARY)
